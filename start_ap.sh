#!/bin/bash

# do not allow network manager to manage the interface
nmcli device set wlan0 managed no

### Make sure we have enough entropy by using the Pi's hardware rng
rngd

### Network setup
ip link set wlan0 down
ip addr add 10.20.30.40/24 dev wlan0
ip link set wlan0 up

### dnsmasq
dnsmasq

### hostapd
/usr/sbin/hostapd -B /etc/hostapd/hostapd.conf

echo "AP initialization complete."

for (( ; ; ))
do
    sleep 1
done
